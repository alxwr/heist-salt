def test_default_config(cli_raw_runpy):
    # This test relies on changes in https://gitlab.com/saltstack/pop/heist/-/merge_requests/142
    return
    ret = cli_raw_runpy("salt.proxy", "--config-template")

    assert ret.json.heist.artifact_version == ""
    assert ret.json.heist.artifacts_dir == "/var/tmp/heist/artifacts"
    assert ret.json.heist.auto_service is False
    assert ret.json.heist.checkin_time == 60
    assert ret.json.heist.clean is False
    assert ret.json.heist.dynamic_upgrade is False
    assert ret.json.heist.generate_keys is True
    assert ret.json.heist.key_plugin == "local_master"
    assert ret.json.heist.manage_service is None
    assert ret.json.heist.offline_mode is False
    assert ret.json.heist.onedir is False
    assert ret.json.heist.renderer == "yaml"
    assert ret.json.heist.retry_key_count == 5
    assert ret.json.heist.retry_key_count == 5
    assert ret.json.heist.roster is None
    assert ret.json.heist.roster_data is None
    assert ret.json.heist.roster_defaults == {}
    assert ret.json.heist.roster_dir == "/etc/heist/rosters"
    assert ret.json.heist.roster_file
    assert ret.json.heist.run_dir_root is False
    assert ret.json.heist.salt_repo_url == "https://repo.saltproject.io/salt/py3/"
    assert ret.json.heist.service_plugin == "raw"
    assert ret.json.heist.target == ""
    # assert ret.json.heist.tunnel_plugin == "asyncssh"


def test_help(cli_raw_runpy):
    """
    Verify that the salt minion heist manager is callable from the cli
    """
    ret = cli_raw_runpy("salt.proxy", "--help", parse_output=False)
    print(ret.stdout)


def test_basic(cli_runpy, roster):
    return
    roster["localhost"] = {}
    ret = cli_runpy("salt.proxy")
    print(ret)
