import tempfile

import yaml

from tests.integration.manager.salt.conftest import wait_for_log_message


async def test_minion_master(
    cli_spawn,
    # minion_container,
    master_container,
):
    # Create a roster just for the salt master
    with tempfile.NamedTemporaryFile(suffix=".yml") as fh:
        fh.write(yaml.dump(dict(master=dict(master_container))).encode())
        fh.flush()
        proc_master = await cli_spawn("salt.master", "-R", fh.name, "--log-level=info")

        # Check the logs to verify that heist ran successfully for the master
        await wait_for_log_message(proc_master, "Opening SSH connection")

        # Wait for the binary to download and deploy, then start salt master
        await wait_for_log_message(
            proc_master, "Starting the service salt-master", timeout=60
        )

        # At this point everything is up and running perfectly
        await wait_for_log_message(proc_master, "Received exit status 0")

    # Create a roster just for the salt minion
    with tempfile.NamedTemporaryFile(suffix=".yml") as fh:
        fh.write(yaml.dump(dict(master=dict(minion_container))).encode())
        fh.flush()
        proc_master = await cli_spawn("salt.master", "-R", fh.name, "--log-level=info")

        # Check the logs to verify that heist ran successfully for the master
        await wait_for_log_message(proc_master, "Opening SSH connection")

        # Wait for the binary to download and deploy, then start salt master
        await wait_for_log_message(proc_master, "Getting target grains")

        # At this point everything is up and running perfectly
        await wait_for_log_message(proc_master, "Received exit status 0")
