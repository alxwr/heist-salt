# /usr/bin/env python3
import sys
from contextlib import redirect_stdout
from io import StringIO

import pop.hub

if __name__ == "__main__":
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="output")
    hub.pop.sub.add(dyne_name="heist")

    with StringIO() as buffer, redirect_stdout(buffer):
        retcode = hub.heist.init.cli()
        stdout = buffer.getvalue().strip()

    # targets = hub.heist.TARGETS
    # rosters = dict(hub.heist.ROSTERS)
    # connections = hub.heist.CONS

    ret = {
        "stdout": stdout,
        # "targets": targets,
        "targets": {},
        # "rosters": rosters,
        "rosters": {},
        # "connections": connections,
        "connections": {},
    }

    print(hub.output.json.display(ret))
    sys.exit(retcode)
