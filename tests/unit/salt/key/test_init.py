import pathlib
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import pytest
from dict_tools.data import NamespaceDict


@pytest.mark.parametrize(
    "sudo",
    [True, False],
)
@pytest.mark.parametrize("target_os", ["linux", "windows"])
@pytest.mark.asyncio
async def test_generate_keys(hub, mock_hub, tmp_path, sudo, target_os):
    """
    test generate_keys
    """
    mock_hub.salt.key.init.generate_keys = hub.salt.key.init.generate_keys
    test_run_dir = pathlib.Path("test_run_dir")
    mock_hub.tool.artifacts.get_salt_path = hub.tool.artifacts.get_salt_path
    key_dir = test_run_dir / "root_dir" / "etc" / "salt" / "pki" / "minion"
    if target_os == "windows":
        key_dir = test_run_dir / "root_dir" / "conf" / "pki" / "minion"
    binary = test_run_dir / "salt"
    target_name = "test_target"
    user = "testuser"
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    mock_hub.tunnel.asyncssh.CONS = NamespaceDict({target_name: {"sudo": sudo}})
    mock_hub.OPT = Mock()
    mock_hub.OPT.heist = NamespaceDict(key_plugin="local_master", onedir=False)
    with patch(
        "salt.config.client_config", Mock(return_value={"pki_dir": tmp_path / "pki"})
    ):
        with patch("pathlib.Path.is_file", Mock(return_value=True)):
            assert await mock_hub.salt.key.init.generate_keys(
                target_name=target_name,
                tunnel_plugin="asyncssh",
                run_dir=test_run_dir,
                target_id="test_minion",
                target_os=target_os,
                user=user,
            )
    perms = 700
    exp_salt_call = call(
        target_name,
        f"{binary}/salt-call --local --config-dir {str(test_run_dir / 'root_dir' / 'conf')} seed.mkconfig tmp={key_dir}",
        target_os=target_os,
    )
    if sudo:
        perms = 710
    if target_os == "linux":
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
            target_name,
            f"mkdir -m{perms} -p {str(key_dir.parent.parent.parent)} {str(key_dir.parent.parent)} {str(key_dir.parent)} {str(key_dir)}",
            target_os="linux",
        )
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list[1] == call(
            "test_target", f"chown -R {user}:{user} test_run_dir/root_dir/etc"
        )
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list[2] == exp_salt_call
    else:
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
            "test_target",
            f'powershell -command "New-Item -Path "{str(key_dir)}" -Type Directory; $acl = Get-Acl "{str(key_dir.parent)}"; $acl.SetSecurityDescriptorSddlForm("\'D:PAI(A;OICI;FA;;;OW)(A;OICI;FA;;;SY)(A;OICI;FA;;;BA)\'"); $acl.SetOwner([System.Security.Principal.NTAccount]\'BUILTIN\\Administrators\'); Set-Acl -Path "{str(key_dir.parent)}" -AclObject $acl"',
            target_os="windows",
        )
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list[1] == exp_salt_call
