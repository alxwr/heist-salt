import pathlib
from unittest.mock import Mock
from unittest.mock import patch

import yaml


def test_generate_pillar(hub, mock_hub, tmp_path):
    """
    test generating pillar data
    """
    target_id = "test_name"
    exp_data = {
        "pillar": {
            "test1": "data1",
            "test2": "data2",
            "test3": "data3",
            "test4": ["data4"],
        }
    }
    mock_hub.salt.pillar.init.generate_pillar = hub.salt.pillar.init.generate_pillar

    with patch(
        "salt.config.client_config",
        Mock(return_value={"pillar_roots": {"base": [tmp_path]}, "pillarenv": "base"}),
    ):
        assert mock_hub.salt.pillar.init.generate_pillar(target_id, exp_data)

    with open(tmp_path / f"{target_id}.sls") as fp:
        sls_data = yaml.safe_load(fp)
    assert sls_data == exp_data


def test_generate_top(hub, mock_hub, tmp_path):
    """
    test generating top data
    """
    target_id = "test_name"
    pillar_env = "base"
    exp_data = {pillar_env: {target_id: [target_id]}}
    pillar_roots = pathlib.Path(tmp_path)
    mock_hub.salt.pillar.init.generate_top = hub.salt.pillar.init.generate_top

    assert mock_hub.salt.pillar.init.generate_top(
        target_id, pillar_roots, pillar_env=pillar_env
    )

    with open(tmp_path / f"top.sls") as fp:
        top_data = yaml.safe_load(fp)
    assert top_data == exp_data
