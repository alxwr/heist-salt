import json
import pathlib
import tarfile
import tempfile
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import aiohttp
import pytest
from dict_tools.data import NamespaceDict

from tests.support.helpers import TEST_FILES


@pytest.mark.asyncio
async def test_repo_data(hub, mock_hub):
    """
    test repo_data when json returned
    """
    with open(TEST_FILES / "repo.json") as fp:
        repo_data = json.load(fp)
    mock_hub.artifact.salt.repo_data = hub.artifact.salt.repo_data
    mock_hub.artifact.init.fetch.return_value = repo_data
    mock_hub.OPT = Mock()
    salt_repo_url = "https://testrepo.com/"
    assert (
        await mock_hub.artifact.salt.repo_data(salt_repo_url=salt_repo_url) == repo_data
    )
    assert mock_hub.artifact.init.fetch.call_args[0][1] == salt_repo_url + "repo.json"


@pytest.mark.asyncio
async def test_repo_data_none_returned(hub, mock_hub):
    """
    test repo_data when None is returned
    """
    mock_hub.artifact.salt.repo_data = hub.artifact.salt.repo_data
    mock_hub.artifact.init.fetch.return_value = None
    mock_hub.OPT = Mock()
    salt_repo_url = "https://testrepo.com/"
    assert not await mock_hub.artifact.salt.repo_data(salt_repo_url=salt_repo_url)
    assert mock_hub.artifact.init.fetch.call_args[0][1] == salt_repo_url + "repo.json"


@pytest.mark.asyncio
async def test_get(hub, mock_hub):
    """
    test salt.get artifact
    """
    mock_hub.artifact.salt.get = hub.artifact.salt.get
    mock_hub.artifact.salt.latest.return_value = False
    mock_hub.artifact.init.fetch.return_value = True
    mock_hub.artifact.init.verify.return_value = True
    mock_hub.tool.artifacts.get_artifact_suffix.return_value = "tar.xz"
    mock_temp_dir = tempfile.TemporaryDirectory()
    artifact = "salt-3006.0-onedir-linux-x86_64.tar.xz"

    # add artifact
    with tempfile.TemporaryDirectory() as repo_tmpdir:
        with tarfile.open(pathlib.Path(mock_temp_dir.name, artifact), "w:gz") as tf:
            tf.add(TEST_FILES / "repo.json")

        with open(TEST_FILES / "repo.json") as fp:
            repo_data = json.load(fp)

        artifacts_dir = pathlib.Path(repo_tmpdir) / "repo"
        mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts_dir
        mock_hub.OPT.heist = NamespaceDict(
            artifacts_dir=artifacts_dir,
            salt_repo_url="https://testrepo.com",
            onedir=False,
        )
        async with aiohttp.ClientSession() as session:
            with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
                tmp_path = await mock_hub.artifact.salt.get(
                    "linux",
                    version="3006.0",
                    repo_data=repo_data,
                    session=session,
                    tmpdirname=pathlib.Path(mock_temp_dir.name),
                )
        assert tmp_path.exists()
        assert pathlib.Path(mock_temp_dir.name) / artifact == tmp_path


@pytest.mark.parametrize(
    "sudo,exp_perms",
    [
        (True, 710),
        (False, 700),
    ],
)
@pytest.mark.asyncio
async def test_deploy(hub, mock_hub, tmpdir, sudo, exp_perms):
    """
    test the correct path is returned when running test_deploy
    """
    mock_hub.artifact.salt.deploy = hub.artifact.salt.deploy
    mock_hub.artifact.salt.extract_binary = hub.artifact.salt.extract_binary
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    mock_hub.heist.salt.minion.get_salt_opts.return_value = ("conf_file", {})
    mock_hub.OPT.heist = NamespaceDict(onedir=False, artifacts_dir="")
    mock_tempfile = tempfile.TemporaryDirectory()
    run_dir = pathlib.Path("var") / "tmp" / "test_run_dir"
    tunnel_name = "test_tunnel_name"
    test_minion = "test_minion"
    user = "testuser"
    run_dir_root = "/tmp"
    mock_hub.tunnel.asyncssh.CONS = NamespaceDict({tunnel_name: {"sudo": sudo}})

    with tempfile.TemporaryDirectory() as repo_tmpdir:
        repo_tmpdir = pathlib.Path(repo_tmpdir)
        mock_hub.tool.artifacts.get_artifact_dir.return_value = str(repo_tmpdir)
        binary = repo_tmpdir / "salt-3003-3-linux-amd64.tar.gz"
        with tarfile.open(binary, "w:gz") as tf:
            tf.add(TEST_FILES / "repo.json")

        patch_os = patch("os.remove", Mock(return_value=True))
        patch_tempfile = patch(
            "tempfile.TemporaryDirectory", Mock(return_value=mock_tempfile)
        )
        minion_dir = repo_tmpdir / test_minion
        minion_dir.mkdir()
        with patch_os, patch_tempfile:
            await mock_hub.artifact.salt.deploy(
                tunnel_name,
                "asyncssh",
                run_dir,
                binary,
                target_id=test_minion,
                user=user,
                run_dir_root=run_dir_root,
            )

        mock_hub.artifact.init.verify_checksum.assert_not_called()
        root_dir = run_dir / "root_dir"
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list == [
            call(
                tunnel_name,
                f"mkdir -m{exp_perms} -p {root_dir.parent.parent} {root_dir.parent} {root_dir}",
                target_os="linux",
            ),
            call("test_tunnel_name", f"chown -R {user}:{user} var/tmp"),
            call(tunnel_name, f"chmod 744 {run_dir / 'salt'}", target_os="linux"),
        ]

        assert mock_hub.tunnel.asyncssh.send.call_args_list[0] == call(
            tunnel_name,
            [binary, str(minion_dir / "root_dir"), str(minion_dir / "code_checksum")],
            root_dir.parent,
            preserve=True,
            recurse=True,
        )

        assert mock_hub.heist.salt.minion.get_salt_opts.call_args_list[0] == call(
            run_dir,
            tunnel_name,
            target_os="linux",
            target_id=test_minion,
            bootstrap=False,
            run_dir_root="/tmp",
        )


@pytest.mark.asyncio
async def test_deploy_verify(hub, mock_hub, tmp_path):
    """
    test deploy when verify is true
    """
    mock_hub.artifact.salt.deploy = hub.artifact.salt.deploy
    run_dir = pathlib.Path("var") / "tmp" / "test_run_dir"
    tunnel_name = "test_tunnel_name"
    test_minion = "test_minion"
    user = "testuser"

    binary = tmp_path / "salt-3003-3-linux-amd64.tar.gz"
    artifacts_dir = tmp_path / "artifacts_dir"
    mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts_dir
    await mock_hub.artifact.salt.deploy(
        tunnel_name,
        "asyncssh",
        run_dir,
        binary,
        target_id=test_minion,
        user=user,
        verify=True,
    )

    mock_hub.artifact.init.verify_checksum.assert_called()

    assert mock_hub.artifact.init.verify_checksum.call_args == call(
        target_name=tunnel_name,
        tunnel_plugin="asyncssh",
        run_dir=run_dir,
        source_fp=artifacts_dir / test_minion / "code_checksum",
        target_fp=run_dir / "code_checksum",
        target_os="linux",
    )


@pytest.mark.parametrize(
    "versions,exp_ver",
    [
        (["3006.0", "3006.0rc1"], "3006.0"),
    ],
)
def test_latest(hub, mock_hub, tmp_path, versions, exp_ver):
    """
    test query the latest with different version numbers
    """
    artifacts_dir = tmp_path / "onedir"
    artifacts_dir.mkdir()
    for version in versions:
        with open(artifacts_dir / f"salt-{version}-linux-amd64.tar.gz", "w"):
            pass
    mock_hub.artifact.salt.latest = hub.artifact.salt.latest
    mock_hub.tool.artifacts.get_artifact_dir.return_value = str(artifacts_dir)
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=tmp_path, onedir=False)
    ret = mock_hub.artifact.salt.latest("salt")
    assert ret == str(artifacts_dir / f"salt-{exp_ver}-linux-amd64.tar.gz")


def test_generate_checksum(hub, mock_hub, tmp_path):
    """
    test generating the checksum data
    """
    artifacts_dir = tmp_path / "artifacts"
    binary = artifacts_dir / "salt-3000-1.tar.gz"
    checksum_file = artifacts_dir / "code_checksum"
    minion_id = "test-minion"
    conf_dir = artifacts_dir / minion_id / "root_dir" / "conf"
    conf_dir.mkdir(parents=True)
    conf_file = conf_dir / "minion"
    conf_file.touch()
    scripts_dir = artifacts_dir / "scripts"
    scripts_dir.mkdir(parents=True)
    alias = scripts_dir / "salt-call"
    alias.touch()
    mock_hub.artifact.salt.generate_checksum = hub.artifact.salt.generate_checksum
    mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts_dir
    mock_hub.artifact.init.checksum = hub.artifact.init.checksum
    with tarfile.open(binary, "w:gz") as tf:
        tf.add(TEST_FILES / "repo.json")
    ret = mock_hub.artifact.salt.generate_checksum(
        binary, checksum_file, minion_id, files=[scripts_dir], target_os="linux"
    )
    with open(ret) as fp:
        content = fp.read()
    assert binary.name in content.split()


@pytest.mark.parametrize(
    "target_os",
    [
        "linux",
        "windows",
    ],
)
@pytest.mark.asyncio
async def test_extract_binary(hub, mock_hub, tmp_path, target_os):
    """
    test extracting the binary
    """
    run_dir = tmp_path / "run_dir"
    run_dir.mkdir()
    artifacts_dir = tmp_path / "artifacts"
    artifacts_dir.mkdir()
    binary = artifacts_dir / "salt-3000-1.tar.gz"
    binary_path = run_dir / "salt"
    target_name = "test-name"
    tunnel_plugin = "asyncssh"
    mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts_dir
    mock_hub.artifact.salt.extract_binary = hub.artifact.salt.extract_binary
    with tarfile.open(binary, "w:gz") as tf:
        tf.add(TEST_FILES / "repo.json")

    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    ret = await mock_hub.artifact.salt.extract_binary(
        target_name, tunnel_plugin, binary, binary_path, run_dir, target_os=target_os
    )

    assert mock_hub.artifact.init.extract.call_args == call(
        target_name,
        tunnel_plugin=tunnel_plugin,
        binary=binary,
        run_dir=run_dir,
        target_os=target_os,
    )
    exp_cmd = f"chmod 744 {binary_path}"
    if target_os == "windows":
        sddl = "'D:PAI(A;OICI;0x1200a9;;;WD)(A;OICI;FA;;;SY)(A;OICI;FA;;;BA)'"
        owner = r"[System.Security.Principal.NTAccount]'BUILTIN\Administrators'"
        exp_cmd = (
            "powershell -command "
            + '"'
            + "; ".join(
                [
                    f'$acl = Get-Acl "{binary_path}"',
                    f'$acl.SetSecurityDescriptorSddlForm("{sddl}")',
                    f"$acl.SetOwner({owner})",
                    f'Set-Acl -Path "{binary_path}" -AclObject $acl',
                ]
            )
            + '"'
        )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
        target_name, exp_cmd, target_os=target_os
    )


@pytest.mark.parametrize(
    "target_os",
    [
        "linux",
        "windows",
    ],
)
@pytest.mark.asyncio
async def test_update(hub, mock_hub, tmp_path, target_os):
    """
    test updating the artifact
    """
    run_dir = tmp_path / "run_dir"
    run_dir.mkdir()
    service_plugin = "raw"
    service_name = "salt"
    artifacts_dir = tmp_path / "artifacts"
    artifacts_dir.mkdir()
    old_binary = artifacts_dir / "salt-3000-1.tar.gz"
    new_binary = artifacts_dir / "salt-3001-1.tar.gz"
    binary_path = run_dir / "salt"
    target_name = "test-name"
    tunnel_plugin = "asyncssh"
    run_cmd = f"{binary_path} -c /tmp/conf"
    mock_hub.artifact.salt.update = hub.artifact.salt.update
    mock_hub.heist.salt.minion.get_service_name.return_value = service_name
    mock_hub.heist.salt.minion.raw_run_cmd.return_value = run_cmd
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    ret = await mock_hub.artifact.salt.update(
        target_name,
        tunnel_plugin,
        service_plugin,
        run_dir,
        "minionid",
        old_binary=old_binary,
        new_binary=new_binary,
        binary_path=binary_path,
        target_os=target_os,
    )
    exp_rmdir = f"rm -rf {binary_path}"
    exp_rmbin = f"rm {str(run_dir / old_binary.name)}"
    if target_os == "windows":
        exp_rmdir = f"cmd /c rmdir /s /q {binary_path}"
        exp_rmbin = f"cmd /c del /s /q {str(run_dir / old_binary.name)}"

    assert mock_hub.tunnel.asyncssh.cmd.call_args_list == [
        call(target_name, exp_rmdir, target_os=target_os),
        call(target_name, exp_rmbin, target_os=target_os),
    ]
    assert mock_hub.artifact.salt.deploy.call_args == call(
        target_name=target_name,
        tunnel_plugin=tunnel_plugin,
        run_dir=run_dir,
        binary=new_binary,
        target_os=target_os,
        target_id="minionid",
        update=True,
    )
    assert mock_hub.service.init.restart.call_args == call(
        target_name,
        tunnel_plugin,
        service=service_name,
        run_cmd=run_cmd,
        target_os=target_os,
        run_dir=run_dir,
    )


@pytest.mark.parametrize(
    "target_os",
    [
        "linux",
        "windows",
    ],
)
@pytest.mark.asyncio
async def test_aliases(hub, mock_hub, tmp_path, target_os):
    """
    test salt.aliases function
    """
    target_name = "test-name"
    tunnel_plugin = "asyncssh"
    run_dir = tmp_path / "run_dir"
    scripts_dir = tmp_path / "scripts"
    manager = "minion"
    mock_hub.artifact.salt.aliases = hub.artifact.salt.aliases
    mock_hub.heist.salt.minion.generate_aliases = hub.heist.salt.minion.generate_aliases
    mock_hub.tool.artifacts.get_artifact_dir.return_value = tmp_path

    ret = await mock_hub.artifact.salt.aliases(
        target_name, tunnel_plugin, run_dir=run_dir, target_os=target_os
    )

    assert (
        mock_hub.artifact.init.create_aliases.call_args[1]["aliases"]["salt-call"][
            "file"
        ]
        == scripts_dir / manager / "salt-call"
    )
    assert (
        mock_hub.artifact.init.create_aliases.call_args[1]["aliases"]["salt-minion"][
            "file"
        ]
        == scripts_dir / manager / "salt-minion"
    )
    assert ret == scripts_dir / manager


@pytest.mark.parametrize(
    "sudo,exp_perms",
    [
        (True, 710),
        (False, 700),
    ],
)
@pytest.mark.asyncio
async def test_deploy_update(hub, mock_hub, tmpdir, sudo, exp_perms):
    """
    test the correct path is returned when running test_deploy
    and update is True.
    """
    mock_hub.artifact.salt.deploy = hub.artifact.salt.deploy
    mock_hub.artifact.salt.extract_binary = hub.artifact.salt.extract_binary
    mock_hub.heist.salt.minion.get_salt_opts.return_value = ("conf_file", {})
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    mock_hub.OPT.heist = NamespaceDict(onedir=False, artifacts_dir="")
    mock_tempfile = tempfile.TemporaryDirectory()
    run_dir = pathlib.Path("var") / "tmp" / "test_run_dir"
    tunnel_name = "test_tunnel_name"
    test_minion = "test_minion"
    user = "testuser"
    mock_hub.tunnel.asyncssh.CONS = NamespaceDict({tunnel_name: {"sudo": sudo}})

    with tempfile.TemporaryDirectory() as repo_tmpdir:
        repo_tmpdir = pathlib.Path(repo_tmpdir)
        mock_hub.tool.artifacts.get_artifact_dir.return_value = str(repo_tmpdir)
        binary = repo_tmpdir / "salt-3003-3-linux-amd64.tar.gz"
        with tarfile.open(binary, "w:gz") as tf:
            tf.add(TEST_FILES / "repo.json")

        patch_os = patch("os.remove", Mock(return_value=True))
        patch_tempfile = patch(
            "tempfile.TemporaryDirectory", Mock(return_value=mock_tempfile)
        )
        minion_dir = repo_tmpdir / test_minion
        minion_dir.mkdir()
        with patch_os, patch_tempfile:
            await mock_hub.artifact.salt.deploy(
                tunnel_name,
                "asyncssh",
                run_dir,
                binary,
                target_id=test_minion,
                user=user,
                update=True,
            )

        mock_hub.artifact.init.verify_checksum.assert_not_called()
        root_dir = run_dir / "root_dir"
        assert mock_hub.tunnel.asyncssh.cmd.call_args_list == [
            call(tunnel_name, f"chmod 744 {run_dir / 'salt'}", target_os="linux"),
        ]

        assert mock_hub.tunnel.asyncssh.send.call_args_list[0] == call(
            tunnel_name,
            [binary, str(minion_dir / "root_dir"), str(minion_dir / "code_checksum")],
            root_dir.parent,
            preserve=True,
            recurse=True,
        )

        mock_hub.tool.config.get_minion_opts.assert_not_called()
        mock_hub.artifact.salt.aliases.assert_not_called()
