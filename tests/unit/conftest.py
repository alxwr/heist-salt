from unittest import mock

import pytest
from dict_tools.data import NamespaceDict


@pytest.fixture(scope="function")
def hub(hub):
    for dyne in ("config", "service", "heist", "salt", "artifact", "tool", "tunnel"):
        hub.pop.sub.add(dyne_name=dyne)
    hub.pop.sub.load_subdirs(hub.salt, recurse=True)
    yield hub


@pytest.fixture(scope="function")
def mock_hub(hub):
    m_hub = hub.pop.testing.mock_hub()
    m_hub.OPT = mock.MagicMock()
    m_hub.SUBPARSER = "salt.minion"
    yield m_hub


class ConfigTestData:
    def __init__(self):
        self.tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
        self.tunnel_plugin = "asyncssh"
        self.remote = NamespaceDict(
            host="192.168.1.2",
            username="root",
            password="test_password",
            id="testarget_name",
            tunnel="asyncssh",
        )
        self.roster = {
            self.tname: {
                "host": "192.168.1.59",
                "username": "root",
                "password": "testpasswd",
                "master_port": 1234,
                "minion_opts": {"master_port": 5678},
                "id": "test_id",
                "tunnel": "asyncssh",
            }
        }
        self.CONS = NamespaceDict()
        self.CONS[self.tname] = NamespaceDict()


@pytest.fixture()
def test_data():
    return ConfigTestData()
