import pathlib


def test_get_salt_path(hub, mock_hub):
    """
    Test get_salt_path
    """
    mock_hub.tool.artifacts.get_salt_path = hub.tool.artifacts.get_salt_path
    run_dir = pathlib.Path("rundir")
    assert (
        mock_hub.tool.artifacts.get_salt_path(run_dir, target_os="linux")
        == run_dir / "salt"
    )


def test_target_conf(hub, mock_hub):
    """
    Test target_conf
    """
    mock_hub.tool.artifacts.target_conf = hub.tool.artifacts.target_conf
    run_dir = pathlib.Path("rundir")
    assert mock_hub.tool.artifacts.target_conf(run_dir) == run_dir / "root_dir" / "conf"
