import pytest


@pytest.mark.parametrize(
    "run_cmd,exp_ret",
    [
        ("/tmp/test/salt/salt-minion -c /tmp/test/conf/", True),
        ("/tmp/test/salt foobar -c /tmp/test/conf/", False),
        ("/../salt/salt-minion -c /tmp/test/conf/", False),
        ("/tmp/test/salt/salt-minion -c /../test/conf/", False),
        ("/tmp/test/salt/salt-minion -a /tmp/test/conf/", False),
    ],
)
def test_valid_run_cmd(mock_hub, hub, run_cmd, exp_ret):
    run_dir = "/tmp/test"
    mock_hub.tool.service.valid_run_cmd = hub.tool.service.valid_run_cmd
    mock_hub.tool.path.clean_path = hub.tool.path.clean_path
    ret = mock_hub.tool.service.valid_run_cmd(run_cmd, run_dir=run_dir)
    assert ret is exp_ret
