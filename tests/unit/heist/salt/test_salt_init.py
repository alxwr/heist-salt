import pathlib
import unittest.mock as mock


try:
    from unittest.mock import AsyncMock
except ImportError:
    # AsyncMock was first introduced in 3.8
    AsyncMock = None
from unittest.mock import call
from unittest.mock import Mock

import asyncssh
import pytest
from dict_tools.data import NamespaceDict


@pytest.mark.asyncio
async def test_manage_tunnel(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and no tunnel created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.init.manage_tunnel = hub.heist.salt.init.manage_tunnel
    assert await mock_hub.heist.salt.init.manage_tunnel(
        test_data.tname, "asyncssh", remote=test_data.remote
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    mock_hub.tunnel.asyncssh.tunnel.assert_not_called()


@pytest.mark.asyncio
async def test_manage_tunnel_with_salt_tunnel(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and the salt tunnel created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.init.manage_tunnel = hub.heist.salt.init.manage_tunnel
    mock_hub.heist.salt.minion.tunnel_to_ports = hub.heist.salt.minion.tunnel_to_ports
    config = mock_hub.heist.salt.minion.get_salt_opts(
        run_dir=pathlib.Path("root"),
        target_name="1234",
        target_os="linux",
        target_id="test_minion",
    )
    assert await mock_hub.heist.salt.init.manage_tunnel(
        test_data.tname,
        "asyncssh",
        remote=test_data.remote,
        tunnel=True,
        salt_conf=config,
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    assert mock_hub.tunnel.asyncssh.tunnel.call_count == 2


@pytest.mark.parametrize("py_version", [(3, 8), (3, 9)])
@pytest.mark.asyncio
async def test_run(hub, mock_hub, py_version):
    if not AsyncMock:
        raise pytest.skip("AsyncMock is required to run this test")
    mock_hub.heist.salt.init.check_deps.return_value = True
    mock_hub.heist.salt.minion.run = hub.heist.salt.minion.run
    mock_hub.heist.salt.init.run = hub.heist.salt.init.run
    remotes = {"test_host": {"host": "192.168.1.2"}}
    mock_gather = AsyncMock()
    patch_gather = mock.patch("asyncio.gather", mock_gather)
    patch_version = mock.patch("sys.version_info", Mock(return_value=py_version))
    with patch_gather, patch_version:
        await mock_hub.heist.salt.init.run(remotes=remotes)
    if py_version == (3, 6):
        mock_gather.call_args_list[0][1] == {"return_exceptions": False}
        mock_gather.call_args_list[0][1] == {"loop": hub.pop.Loop}
        len(mock_gather.call_args_list[0]) == 3
    else:
        mock_gather.call_args_list[0][1] == {"return_exceptions": False}
        len(mock_gather.call_args_list[0]) == 2


@pytest.mark.asyncio
async def test_manage_tunnel_with_salt_tunnel_bootstrap(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and the tunnel is true
    but bootstrap is True. It should not create the tunnel.
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.init.manage_tunnel = hub.heist.salt.init.manage_tunnel
    config = mock_hub.heist.salt.minion.get_salt_opts(
        run_dir=pathlib.Path("root"),
        target_name="1234",
        target_os="linux",
        target_id="test_minion",
        bootstrap=True,
    )
    assert await mock_hub.heist.salt.init.manage_tunnel(
        test_data.tname,
        "asyncssh",
        remote=test_data.remote,
        tunnel=True,
        salt_conf=config,
        bootstrap=True,
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    assert mock_hub.tunnel.asyncssh.tunnel.call_count == 0


@pytest.mark.asyncio
async def test_single_offline_mode(hub, mock_hub, test_data, tmp_path):
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.heist.salt.init.single = hub.heist.salt.init.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.tool.artifacts.get_artifact_suffix.return_value = "tar.gz"
    mock_hub.artifact.init.deploy.return_value = (tmp_path / "run_dir", "", False, None)
    await mock_hub.heist.salt.init.single(remote=test_data.remote)
    mock_hub.artifact.salt.repo_data.assert_not_called()
    mock_hub.artifact.init.get.assert_not_called()
    mock_hub.artifact.salt.latest.assert_called_once()


@pytest.mark.asyncio
async def test_single_host_unreachable(hub, mock_hub, test_data):
    """
    test call to single when we cannot connect to target host
    """
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.heist.salt.init.single = hub.heist.salt.init.single
    mock_hub.heist.salt.init.manage_tunnel = hub.heist.salt.init.manage_tunnel
    mock_hub.tunnel.asyncssh.create.return_value = False
    ret = await mock_hub.heist.salt.init.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "comment": f"Could not establish tunnel with {minion_id}",
        "result": "Error",
        "retvalue": 1,
        "target": minion_id,
    }
    mock_hub.tool.system.os_arch.assert_not_called()


@pytest.mark.asyncio
async def test_single_host_artifact_not_downloaded(hub, mock_hub, test_data):
    """
    test call to single when we cannot download an artifact
    """
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.offline_mode = False
    mock_hub.heist.salt.init.single = hub.heist.salt.init.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.salt.deploy.return_value = False
    mock_hub.artifact.salt.repo_data.return_value = {
        "latest": {
            "salt-3004.2-1-linux-amd64.tar.gz": {
                "name": "salt-3004.2-1-linux-amd64.tar.gz",
                "version": "3004.2-1",
                "os": "linux",
            }
        }
    }
    mock_hub.artifact.init.get.return_value = False
    mock_hub.tool.artifacts.get_artifact_dir.return_value = pathlib.Path()
    ret = await mock_hub.heist.salt.init.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "result": "Error",
        "comment": "Could not download the artifact",
        "retvalue": 1,
        "target": minion_id,
    }


@pytest.mark.asyncio
async def test_single_host_not_clean_path(hub, mock_hub, test_data, tmp_path):
    """
    test call to single when run_dir is not a clean_path
    """
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.offline_mode = False
    mock_hub.heist.salt.init.single = hub.heist.salt.init.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.init.deploy.return_value = (
        tmp_path / "run_dir",
        tmp_path / "salt",
        False,
    )
    mock_hub.tool.path.path_convert.return_value = tmp_path
    mock_hub.artifact.salt.repo_data.return_value = {
        "latest": {
            "salt-3004.2-1-linux-amd64.tar.gz": {
                "name": "salt-3004.2-1-linux-amd64.tar.gz",
                "version": "3004.2-1",
                "os": "linux",
            }
        }
    }
    mock_hub.artifact.init.get.return_value = True
    mock_hub.tool.artifacts.get_artifact_dir.return_value = pathlib.Path()
    mock_hub.tool.path.clean_path.return_value = False
    ret = await mock_hub.heist.salt.init.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "result": "Error",
        "comment": f"The run_dir {tmp_path} is not a valid path",
        "retvalue": 1,
        "target": minion_id,
    }


@pytest.mark.parametrize("use_prev", [True, False])
@pytest.mark.asyncio
async def test_single_bootstrap_return(hub, mock_hub, test_data, tmp_path, use_prev):
    """
    test call to single when bootstrap and returns successful
    when an artifact already exists and when it doesn't
    """
    test_data.remote.bootstrap = True
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.offline_mode = True
    mock_hub.heist.salt.init.single = hub.heist.salt.init.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.init.deploy.return_value = (
        tmp_path / "run_dir",
        tmp_path / "salt",
        use_prev,
        "salt-3005-1.tar.gz",
    )
    mock_hub.tool.path.path_convert.return_value = tmp_path
    mock_hub.heist.salt.minion.get_salt_opts.return_value = ("conf_file", {})
    mock_hub.tool.path.clean_path.return_value = True
    mock_hub.OPT.heist.dynamic_upgrade = False
    ret = await mock_hub.heist.salt.init.single(remote=test_data.remote)
    minion_id = test_data.remote["id"]
    assert ret == {
        "result": "Success",
        "comment": f"The target {minion_id} bootstrapped Salt minion successfully",
        "retvalue": 0,
        "target": minion_id,
    }
    if use_prev:
        mock_hub.salt.key.init.generate_keys.assert_not_called()


@pytest.mark.asyncio
async def test_manage_tunnel_with_salt_tunnel_cant_connect(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and the salt tunnel cannot be
    created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.init.manage_tunnel = hub.heist.salt.init.manage_tunnel
    mock_hub.tunnel.asyncssh.tunnel.side_effect = asyncssh.misc.ChannelListenError
    mock_hub.heist.salt.minion.tunnel_to_ports = hub.heist.salt.minion.tunnel_to_ports
    config = mock_hub.heist.salt.minion.get_salt_opts(
        run_dir=pathlib.Path("root"),
        target_name="1234",
        target_os="linux",
        target_id="test_minion",
    )
    assert not await mock_hub.heist.salt.init.manage_tunnel(
        test_data.tname,
        "asyncssh",
        remote=test_data.remote,
        tunnel=True,
        salt_conf=config,
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    assert mock_hub.tunnel.asyncssh.tunnel.call_count == 1


@pytest.mark.asyncio
async def test_single_upgrade(hub, mock_hub, test_data, tmp_path):
    """
    test when an artifact needs to be upgraded
    when initiating a connection with a target
    """
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.dynamic_upgrade = True
    mock_hub.heist.salt.minion.get_salt_opts.return_value = ("conf_file", {})
    pkg_name = "pkg-3000.tar.gz"
    mock_hub.heist.salt.init.single = hub.heist.salt.init.single
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.tool.artifacts.get_artifact_suffix.return_value = "tar.gz"
    mock_hub.artifact.salt.latest.return_value = tmp_path / "pkg-3001.tar.gz"
    mock_hub.tool.artifacts.get_artifact_dir.return_value = tmp_path / "artifacts"
    mock_hub.artifact.init.deploy.return_value = (
        tmp_path / "run_dir",
        tmp_path / "binary.tar.gz",
        True,
        pkg_name,
    )
    test_data.remote.bootstrap = True
    await mock_hub.heist.salt.init.single(remote=test_data.remote)
    mock_hub.artifact.salt.latest.assert_called()
    mock_hub.artifact.salt.update.assert_called()


@pytest.mark.asyncio
async def test_manage_service(hub, mock_hub, test_data, tmp_path):
    """
    test function manage_service
    """
    mock_hub.heist.salt.init.manage_service = hub.heist.salt.init.manage_service
    mock_hub.service.raw.start = hub.service.raw.start
    mock_hub.heist.salt.minion.get_service_name.return_value = "minion"
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    ret = await mock_hub.heist.salt.init.manage_service(
        target_name=test_data.tname,
        tunnel_plugin="asyncssh",
        service_plugin="raw",
        manage_service="start",
        target_id="test-name",
        run_dir=tmp_path,
        target_os="linux",
    )
    assert ret["comment"] == (
        "The service minion was succesfully put into status start. Closing Heist connection.",
    )
    assert ret["result"] == "Success"
    assert ret["retvalue"] == 0


@pytest.mark.parametrize("clean", [True, False])
@pytest.mark.parametrize("use_prev", [True, False])
@pytest.mark.asyncio
async def test_single_clean(hub, mock_hub, test_data, tmp_path, clean, use_prev):
    """
    test single function when clean is set
    """
    test_data.remote.bootstrap = True
    mock_hub.OPT.heist.checkin_time = 1
    mock_hub.OPT.heist.offline_mode = True
    mock_hub.heist.salt.minion.get_salt_opts.return_value = ("conf_file", {})
    mock_hub.heist.salt.init.single = hub.heist.salt.init.single
    mock_hub.heist.salt.minion.clean = hub.heist.salt.minion.clean
    mock_hub.tool.system.os_arch.return_value = ("linux", "x64")
    mock_hub.artifact.init.deploy.return_value = (
        tmp_path / "run_dir",
        tmp_path / "salt",
        use_prev,
        "salt-3005-1.tar.gz",
    )
    mock_hub.tool.path.path_convert.return_value = tmp_path
    mock_hub.tool.path.clean_path.return_value = True
    mock_hub.OPT.heist.dynamic_upgrade = False
    ret = await mock_hub.heist.salt.init.single(remote=test_data.remote, clean=clean)
    minion_id = test_data.remote["id"]
    assert ret == {
        "result": "Success",
        "comment": f"The target {minion_id} bootstrapped Salt minion successfully",
        "retvalue": 0,
        "target": minion_id,
    }

    if clean and use_prev:
        mock_hub.heist.salt.init.clean.assert_called()
        assert mock_hub.artifact.init.deploy.call_count == 2
    else:
        mock_hub.heist.salt.init.clean.assert_not_called()
        assert mock_hub.artifact.init.deploy.call_count == 1
