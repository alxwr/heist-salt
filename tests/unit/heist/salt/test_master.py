from unittest.mock import call

import pytest


@pytest.mark.parametrize("target_os", ["linux", "windows"])
@pytest.mark.parametrize(
    "service_plugin,exp_ret", [("systemd", "salt-master"), ("raw", "salt-master")]
)
def test_get_service_name(hub, mock_hub, service_plugin, exp_ret, target_os):
    """
    Test get_service_name
    """
    mock_hub.heist.salt.master.get_service_name = hub.heist.salt.master.get_service_name
    assert (
        mock_hub.heist.salt.master.get_service_name(service_plugin, target_os=target_os)
        == exp_ret
    )


@pytest.mark.asyncio
async def test_master_run(hub, mock_hub, test_data):
    """
    test the run function for the salt.master
    manager
    """
    mock_hub.heist.salt.master.run = hub.heist.salt.master.run
    await mock_hub.heist.salt.master.run(remotes=test_data.roster)
    assert mock_hub.heist.salt.init.run.call_args == call(
        test_data.roster, artifact_version=None, manage_service=None
    )


@pytest.mark.asyncio
async def test_master_clean(hub, mock_hub, test_data):
    """
    test the clean function for the salt.master
    manager
    """
    master = "test_master"
    mock_hub.heist.CONS = test_data.CONS
    mock_hub.heist.CONS[test_data.tname]["target_id"] = master
    mock_hub.heist.salt.master.clean = hub.heist.salt.master.clean
    await mock_hub.heist.salt.master.clean(
        test_data.tname, test_data.tunnel_plugin, service_plugin="raw", vals={}
    )
    assert mock_hub.heist.salt.init.clean.call_args == call(
        test_data.tname, test_data.tunnel_plugin, "raw", {}
    )


@pytest.mark.parametrize("bootstrap", [True, False])
def test_get_salt_opts_master(hub, mock_hub, test_data, tmp_path, bootstrap):
    """
    test get_salt_opts function for salt.master manager
    """
    run_dir = tmp_path / "run_dir"
    master = "test_master"
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.ROSTERS[test_data.tname]["master_opts"] = {
        "log_level_logfile": "debug"
    }
    mock_hub.heist.salt.master.get_salt_opts = hub.heist.salt.master.get_salt_opts
    conf_file, opts = mock_hub.heist.salt.master.get_salt_opts(
        run_dir,
        test_data.tname,
        target_os="linux",
        target_id=master,
        bootstrap=bootstrap,
    )
    assert opts == {"log_level_logfile": "debug", "root_dir": str(run_dir / "root_dir")}
    assert conf_file == "master"


@pytest.mark.asyncio
async def test_master_tunnel_to_ports(hub, mock_hub, test_data):
    """
    test the tunnel_to_ports function for the salt.master
    manager
    """
    salt_conf = {}
    mock_hub.heist.salt.master.tunnel_to_ports = hub.heist.salt.master.tunnel_to_ports
    assert await mock_hub.heist.salt.master.tunnel_to_ports(
        test_data.tname, "asyncssh", salt_conf
    )


@pytest.mark.parametrize("target_os", ["linux", "windows"])
def test_master_generate_aliases(hub, mock_hub, test_data, tmp_path, target_os):
    """
    test generate_aliases function for salt.master manager
    """
    run_dir = tmp_path / "run_dir"
    artifacts = tmp_path / "artifacts"
    manager = "master"
    mock_hub.heist.salt.master.generate_aliases = hub.heist.salt.master.generate_aliases
    mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts
    aliases, artifacts_dir, content = mock_hub.heist.salt.master.generate_aliases(
        run_dir, target_os=target_os
    )
    assert artifacts_dir == artifacts / "scripts" / manager
    if target_os == "linux":
        assert "export SSL" in content
    else:
        assert "ECHO OFF" in content
    assert aliases["salt-key"]["file"] == artifacts / "scripts" / manager / "salt-key"
    assert aliases["salt"]["file"] == artifacts / "scripts" / manager / "salt"
    assert (
        aliases["salt-master"]["file"]
        == artifacts / "scripts" / manager / "salt-master"
    )
