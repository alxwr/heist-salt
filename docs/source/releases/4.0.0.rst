===================
v4.0.0 (2021-08-18)
===================

Changelog
=========

Deprecated
----------
- Deprecate artifacts.salt.fetch and artifacts.salt.verify_hash in favor of heist.artifacts.init.{fetch,verify}. This will be removed in Heist-Salt version v5.0.0. (#28)


Added
-----

- Allow users to "bootstrap" their minions. This feature will allow a user to deploy a salt minion and point it to a different master. Heist will no longer manage this minion after deployment. (#5)
- Migrate artifact calls to heist/artifact/init.py (#27)
