===================
v5.0.0 (2021-10-25)
===================

Changelog
=========

Removed
-------

- Removed salt.artifacts.salt.fetch in favor of heist.artifacts.init.fetch
  Removed salt.artifacts.salt.verify_hash in favor of heist.artifacts.init.verify (#28)


Fixed
-----

- Allow heist to work with pkg version. For example 3004-1. It will also continue to allow use of old versioning (3004). (#30)
