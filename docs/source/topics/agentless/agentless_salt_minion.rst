.. _agentless:

Agentless Minions
=================

Heist-Salt agentless is the default use case for Heist-Salt. Heist-Salt
will automatically deploy and install the Salt minion for the user and
automatically generate and accept the Salt key. Heist-Salt will also establish
a SSH tunnel connection between the Salt Master and the Salt Minion. Once this
connection is established and the Salt Minion is deployed a user can then use
Salt to manage the minion, with all of the communication over SSH. When the Heist-Salt
process is killed it cleans everything up, including removing the Salt Minion agent,
the Salt keys, and the SSH tunnel.
