Running Heist-Salt as unprivileged user
---------------------------------------

If you are running Heist-Salt as a non root user and using the agentless
mode of Heist-Salt you need to ensure the user running Heist also has the
correct permissions on Salt. Please see the `running Salt as unprivileged user`_
instructions on what permissions need to be set for your user.


.. _running Salt as unprivileged user: https://docs.saltproject.io/en/latest/ref/configuration/nonroot.html
