.. _introduction:


What is Heist-Salt
==================

Salt requires an agent be installed across all devices a user desires to manage.
The agent normally requires a manual step to install across all devices, before
being able to get started with Salt. Heist-Salt is a tool that removes this manual
installation step and manages the installation for the user. Heist-Salt uses the
`heist`_ tool to help manage the artifacts.

Heist Salt is a tool that can manage the deployment and installation of Salt or be
used as an agentless solution for Salt. Heist-Salt uses Heist, which is a tool that
helps manage any artifacts. Heist-Salt uses Heist to help deploy and install the Salt
artifact. Heist-Salt can currently manage the following artifacts:

 - :ref:`salt-minion`
 - :ref:`salt-master`
 - :ref:`salt-proxy`

There are two different use for Heist-Salt:

 - :ref:`agentless`: Provides an agentless solution for the user over SSH. This removes
    the requirement to for the user to install an agent across each device they want
    to manage with Salt.
 - :ref:`bootstrap`: A solution to automate the installation of the Salt onedir across
   all devices they want to manage. This solution can also manage upgrades of Salt,
   manage the configurations and manage the service.


.. _heist: https://heist.readthedocs.io
