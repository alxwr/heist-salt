Getting Started
===============

Prerequisites
-------------

* Python 3.9+
* git *(if installing from source, or contributing to the project)*

Installation
------------

.. note::

   If wanting to contribute to the project, and setup your local development
   environment, see the ``CONTRIBUTING.rst`` document in the source repository
   for this project.

If wanting to use ``heist-salt``, you can do so by either
installing from PyPI or from source.

Install from PyPI
+++++++++++++++++

To install the latest version from PyPI:

.. code-block:: bash

    # Requires Python 3.9+
    pip install heist-salt

Install from PyPi when using Salt's onedir packages
+++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: bash

   salt-pip install heist-salt

.. note::

   Due to this issue: https://github.com/saltstack/salt/issues/64192
   you cannot run Heist-Salt alongside Salt's 3006.0 onedir packages.
   This was resolved in Salt's 3006.1 onedir packages.

After heist-salt is installed into the Salt's onedir packages, you
need to ensure you add the Heist binary to your path. If you installed
the linux 3006.1 Salt onedir packages, by default this path would be
``/opt/saltstack/salt/extras-3.10/bin/``. This is where the heist binary
is installed when running ``salt-pip install heist-salt``. You could
also directly call the full binary path instead of adding it to your path.
For example:

.. code-block:: bash

   # /opt/saltstack/salt/extras-3.10/bin/heist salt.minion -R /etc/heist/roster

Install from source
+++++++++++++++++++

``heist-salt`` can also be installed from source:

.. code-block:: bash

   # Requires git and Python 3.6+
   git clone https://gitlab.com/saltstack/pop/heist-salt.git
   cd heist-salt
   pip install -e .

Setting up a Salt master
------------------------

Don't worry, this is a snap!  Once Heist is installed you will need a
Salt master to connect to if you are using the ``salt.minion`` manager.
If you have an existing Salt master running or you are using a different
manager such as ``salt.master`` you can skip this section,
just run ``heist`` on your Salt master.

Using onedir binary
-------------------

Download the all-in-one Salt binary for Linux x86_64.

.. code-block:: bash

    wget https://repo.saltproject.io/salt/py3/onedir/latest/salt-3006.1-onedir-linux-x86_64.tar.xz

This is to install the 3006.1 version of Salt. You can view the directory listing here:
https://repo.saltproject.io/salt/py3/onedir/ to see all of the Salt versions available for download.

Extract the tarball:

.. code-block:: bash

   tar -xvf salt-3006.1-onedir-linux-x86_64.tar.xz

This will extract a single file named `salt`. You can now use this single binary to
run the Salt master.

.. code-block:: bash

    ./salt/salt-master

Now you have a running Salt master to control your minions!

Using onedir system packages
----------------------------

You can also install the onedir system packages. Please see the `Salt install guide`_
for instructions on how to install and run the Salt master.

Pip install Salt
----------------

If you want to pip install Salt you only need to run:

.. code-block:: bash

   pip install salt


Now you can run the salt-master in the background:

.. code-block:: bash

   salt-master -d

Making your roster
------------------

A Roster is a file used by Heist to map login information to the
systems in your environment. This file can be very simple and just
needs to tell Heist where your systems are and how to log into them
via ssh. Open a file called ``roster.cfg`` and add the data needed to connect
to a remote system via ssh:

.. code-block:: yaml

    192.168.4.4:
      username: fred
      password: freds_password

The roster files typically all live inside of a roster directory. But to get
started will execute a single roster file with ``heist``:

.. code-block:: bash

    heist <heist-manager> -R roster.cfg

Please see `heist's roster`_ documentation for more information on rosters.


To use the ``salt.minion`` manager to deploy and manage a Salt Minion artifact,
run the following:

.. code-block:: bash

    heist salt.minion -R roster.cfg


To use the ``salt.master`` manager to deploy and manage a Salt Master artifact,
run the following:

.. code-block:: bash

    heist salt.master -R roster.cfg


Assuming your roster is correct, heist will now connect to the remote
system, deploy a Salt minion, and connect it to your running master! Now you
can use the same binary that you started the master with to accept your new
minion's keys:

.. code-block:: bash

    ./salt/salt-key -A

Then give your minion a few seconds to authenticate and then run your first
``salt`` command on the newly set up minion:

.. code-block:: bash

    ./salt/salt \* test.version

That's it! Now that the minion is up you can run ``salt`` commands on it at breakneck
speed, the full power of Salt is at your fingertips!!

.. _Salt install guide: https://docs.saltproject.io/salt/install-guide/en/latest/
.. _heist's roster: https://heist.readthedocs.io/en/latest/topics/rosters.html
