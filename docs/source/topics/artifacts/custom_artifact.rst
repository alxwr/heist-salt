.. _custom_artifact:

Building Custom Salt Bins
=========================

To build a custom Salt binary, follow the instructions in the `Salt packaging`_
documentation.

salt_repo_url
-------------
By default, heist-salt will query https://repo.saltproject.io/salt/py3/onedir/repo.json
to return data about the artifacts. The returned json data will include the artifact name,
version and hashes of the artifact.

You can set a custom repo by setting `salt_repo_url` to a url that points to your custom
repo. The custom repo needs to include a `repo.json` file and follow the directory structure of
https://repo.saltproject.io/salt/py3/onedir/


.. _Salt packaging: https://docs.saltproject.io/en/master/topics/packaging/
