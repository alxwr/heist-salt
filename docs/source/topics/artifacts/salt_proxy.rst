.. _salt-proxy:

Heist Proxy Minion
==================

You can use the ``salt.proxy`` Heist plugin to deploy and manage
a Salt proxy minion artifact.


.. code-block:: bash

    heist salt.proxy -R roster.cfg

This will automatically download and deploy a Salt proxy minion to the
defined targets. This Heist manager will also handle the Salt proxy minion upgrades
and managing the service.


If you want to define Salt proxy config options to add to the proxy config file,
you would define them in your roster file like so:

.. code-block:: yaml

    system_name:
      host: 192.168.1.2
      username: root
      password: "rootpassword"
      proxy_opts:
        log_level_logfile: debug

To define the proxy type and any other needed settings for the Salt proxy minion
you will need to use the ``pillar`` arg in the roster file. For example:

.. code-block:: yaml

    system_name:
      host: 192.168.1.2
      username: root
      password: "rootpassword"
      pillar:
        proxy:
          proxytype: dummy

This setting ensures the pillar data is set for this minion to configure the Salt proxy
minion type to dummy. This will also edit your top file to ensure this pillar data
is added to your pillar environment. It will use the ``base`` pillar environment by default
or look at your Salt opts for ``pillarenv``.


Heist Proxy Minion Grains
-------------------------

There are two ways to differentiate between a Heist-Salt proxy minion
and a regular proxy minion. A Heist-Salt minion communicates with the
master over SSH. Also, a Heist-Salt minion includes a `minion_type: heist_proxy`
grain. If you want to target only heist proxy minions, you can with
grains targeting on a Salt Master.


.. code-block:: bash

    salt -G 'minion_type:heist_proxy' test.version


Since Heist-Salt proxy minions communicate over SSH, if your Salt Master is attempting
to check for connected minions, it will not work by default for Heist Minions. You
will need to set `detect_remote_minions` to True in your Salt Master configuration.
This will check for connections the Master is connected to over port 22
by default. If you are running SSH on a different port, you can change the
port with `remote_minions_port`. These settings will allow presence events, the
manage runner and any other features that detect connected minions to a Salt Master
to work properly.
